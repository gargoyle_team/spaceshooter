﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

    public float speed;
    public bool shooting = false;
    private Vector3 iniPos;
	private float currentTime;
	public float timeLaunch;
	public AudioSource audioLaser;

    public void Shot(Vector3 position, float direction)
    {
		audioLaser.Play();
        transform.position = position;
        shooting = true;
        transform.rotation = Quaternion.Euler(0, 0, direction);
    }

    // Update is called once per frame
    protected void Update()
    {
		currentTime += Time.deltaTime;
		if (currentTime > timeLaunch) {
			Reset();
		}
        if(shooting)
        {
            transform.Translate(0, speed * Time.deltaTime, 0);
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Finish" || other.tag == "PlayerShip")
        {
            Reset();
        }

    }

    public void Reset()
    {
		Destroy (gameObject);
        shooting = false;
    }
}
