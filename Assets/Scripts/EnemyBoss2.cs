﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class EnemyBoss2 : MonoBehaviour {
    public float speed;
    private Vector2 axis;
    public Vector2 limits;
    public Propeller propeller;
    public ParticleSystem explosion;
    public ParticleSystem blueExplosion;
    public GameObject graphics;
    public AudioSource audio;
    private BoxCollider2D myCollider;
    public bool canShoot = true;
    public float direction = 4.0f;
    public float shieldDuration = 4000;
    public Slider enemyLife;
    public GameObject shield;
    private int halfLife = 0;
    private bool death = false;
    void Awake()
    {
        myCollider = GetComponent<BoxCollider2D>();
        enemyLife.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y >= 2.00f)
        {
            transform.Translate(0, -4.0f * Time.deltaTime, 0);
        }
        else
        {

            if (death == false)
            {
                transform.Translate(axis * speed * Time.deltaTime);

                if (transform.position.x > limits.x)
                {
                    direction = -4.0f;
                }
                else if (transform.position.x < -limits.x)
                {
                    direction = 4.0f;
                }
                transform.Translate(direction * Time.deltaTime, 0, 0);
                if (enemyLife.value <= 0)
                {
                    Death();
                    SceneManager.LoadScene("Finish");
                }
            }
        }

    }

    public void SetAxis(Vector2 currentAxis)
    {
        axis = currentAxis;
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
            if (shield.activeSelf == false)
            {
                explosion.Play();
                enemyLife.value -= .003f;
            }
            else
            {
                blueExplosion.Play();
                shieldDuration -= 10;
            }
            if (enemyLife.value <= 0.5 && halfLife == 0)
            {
                Shield();
                halfLife = 1;
            }
            else if (shieldDuration <= 0)
            {
                DesactivateShield();
                shieldDuration = 4000;
            }
        }
    }
    private void Explode()
    {
        graphics.SetActive(false);
        myCollider.enabled = false;
        explosion.Play();
        audio.Play();
        canShoot = false;
    }
    private void Shield()
    {
        shield.SetActive(true);
    }
    private void DesactivateShield()
    {
        shield.SetActive(false);
    }
    private void Attack()
    {

    }

    private void Death()
    {
        death = !death;
        Explode();
        SpaceManager.instance.addHighscore(10000);
        enemyLife.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
}
