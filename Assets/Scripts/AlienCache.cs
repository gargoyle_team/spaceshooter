﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienCache
{
    private Alien[] aliens;
    private int currentAlien = 0;


    public AlienCache(GameObject prefabAlien, Vector3 position, Transform parent, int numAliens)
    {
        aliens = new Alien[numAliens];

        Vector3 tmpPosition = position;
        for(int i = 0; i < numAliens; i++)
        {
            aliens[i] = GameObject.Instantiate(prefabAlien, tmpPosition, Quaternion.identity, parent).GetComponent<Alien>();
            aliens[i].name = prefabAlien.name + "_larva_" + i;
            tmpPosition.x += 1;
        }
    }

    public Alien GetAlien()
    {
        if(currentAlien > aliens.Length - 1)
        {
            currentAlien = 0;
        }

        return aliens[currentAlien++];
    }
}