﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SpaceManager : MonoBehaviour {

    public static SpaceManager instance;
	public int highscore;
    public int lifeplayer;
    public Text highscoreText;
    public Text lifeText;
    public GameObject GameOver;
    public GameObject Pause;
	public GameObject Enemy;
    public GameObject Enemy2;
    public GameObject Alien;
	public GameObject Meteor;
    public GameObject Finish;
    public bool clear1=false;
	public bool clear2=false;
	public bool lifeup=false;
	public GameObject powerUp;
    public GameObject powerUp2;
    public GameObject powerUp3;
    // Use this for initialization
    void Start () {
        instance = this;
        highscore = 0;
        lifeplayer = 10;
        lifeText.text = "x" + lifeplayer.ToString();
        highscoreText.text = highscore.ToString("D5");
    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Pauseg();
        }
		if (highscore >= 5000 && highscore <=5200 && lifeup==false)
        {
            powerUp2.SetActive(true);
        }
        if (highscore>=9000 && clear1!=true)
		{
			clear1 = true;
			EnemyAppear();
		}
    	if(highscore>=100000 && clear2!=true)
		{
			clear2 = true;
            Enemy2Appear();
        }
		if(clear1==true && clear2==true && Enemy2.activeSelf==false){
			Finishg ();
		}
    }



    public void addHighscore(int value)
    {
        highscore += value;
        highscoreText.text = highscore.ToString("D5");
    }

    public void sublive()
    {
        if(lifeplayer > 0)
        {
            lifeplayer--;
            lifeText.text = "X" + lifeplayer.ToString();     
        }
        else
        {
            GameOverg();
        
        }
    }

	public void addlive()
	{

		lifeplayer++;
		lifeText.text = "X" + lifeplayer.ToString();     

	}

	public void EnemyAppear()
	{
		Enemy.gameObject.SetActive (true);
		powerUp.SetActive (true);
    }

    public void Enemy2Appear()
    {
        Enemy2.gameObject.SetActive(true);
        powerUp2.SetActive(true);
    }


    public void Pauseg()
    {
        Pause.SetActive(!Pause.activeSelf);
        if(Pause.activeSelf == true)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

    }
    public void GameOverg()
    {
        GameOver.SetActive(!GameOver.activeSelf);


        if(GameOver.activeSelf == true)
        {
            Debug.Log("GameOver");
            SceneManager.LoadScene("GameOver");
        }
        else
        {
            Time.timeScale = 1;
        }
    }
    public void Finishg()
    {

            SceneManager.LoadScene("Finish");

    }

}
